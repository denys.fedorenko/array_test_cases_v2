**Getting started**

1. open terminal in test cases folder(clone if you dont have)
2. go to ```development``` branch
3. pull all changes from remote
4. go to web and create own repository(the same as this, but empty)
5. add one more ```remote``` like this (my_repo is just an example of the name, link should lead to your repo)
```
  git remote --add <my_repo> <your_repo>
```
6. use only ssh for all operation(no https) 
7. push this repo to your repository
```
  git push my_repo development
```
8. install ```node_modules``
9. check if eslint highlight all error
10. checkout to new branch ```fix/lint_error_tests```
11. fix all lint error and commit your changes
12. create pull request to ```development```
  - ask for accepting it and remove previos branch
***Add 3 your team partner as collaborator(ask me names) to this repo, he will accept your PR***
 - set up this ***[settings](https://help.github.com/articles/enabling-required-reviews-for-pull-requests/)*** to 2
 - reviewer has to left some comments after review
13. checkout to ```development``` branch and pull changes
14. checkout to new ```feature/array_implementation``` branch
15. put code of your Array implementation to ```index.js```
16. fix all lint error and commit your changes
  - push it
  - PR to develop
  - after PR applied checkout to develop and pull
17. checkout to new ```fix/some_method``` branch
18. apply changes for this as one commit or several
    - one commit for one error
    - if this changes fix several tests, list all cases in this commit (below - **example for 3 cases changes**)
    ```
    git commit -m"fix/some_method
      * check length of ...
      * must be a function
      * should calls 3 time"
    "
    ```
19. commit and push, create pull request to ```development```
20. ask for accepting it and remove previous branch
21. repeat steps `17-20`

   ***I hope you'll have fun this time.***