function MyArray(arrayLength, ...args) {
  if (!new.target) {
    return new MyArray(arrayLength, ...args);
  }

  if (Number.isNaN(arrayLength)) {
    throw new RangeError('Invalid array length');
  }

  if (arrayLength < 0 && args.length === 0) {
    throw new RangeError('Invalid array length');
  }

  if (2 ** 32 - 1 < arrayLength + 1) {
    throw new RangeError('Invalid array length');
  }

  if (arguments.length === 1) {
    if (Number.isInteger(arrayLength) && arrayLength >= 0) {
      this.length = arrayLength;
    } else {
      this[0] = arrayLength;
      this.length = 1;
    }
  }

  if (arguments.length === 0) {
    this.length = 0;
  }

  if (args.length !== 0) {
    this[0] = arrayLength;
    this.length = 1;

    for (let i = 1; i <= args.length; i++) {
      this.length += 1;
      this[i] = args[i - 1];
    }
  }
  Object.defineProperty(this, 'length', {
    enumerable: false
  });

  return new Proxy(this, {
    set(obj, prop, value) {
      if (prop === 'length') {
        if (typeof value !== 'number') {
          throw new RangeError('Invalid array length');
        }
        else if (obj.length > value) {
          for (let i = obj.length - 1; i >= value; i--) {
            delete obj[i];
          }
        }
        obj[prop] = value;
      } else {
        obj[prop] = value;
        const newLength = Number(prop) ? Math.abs(Number(prop)) : obj.length;

        if (obj.length < newLength) {
          obj.length = newLength + 1;
        }
      }
    },

    get(obj, prop) {
      return obj[prop];
    }
  });
}


MyArray.prototype.push = function (firstArg, ...args) {
  if (typeof this.length !== 'number' || !this.length) {
    this.length = 0;
  }

  if (arguments.length) {
    const index = this.length;
    this[index] = firstArg;
    this.length += 1;

    for (const el of args) {
      const index = this.length;
      this[index] = el;
      this.length += 1;
    }
  }
  return this.length;
};

MyArray.prototype.pop = function () {
  if (!this.length) {
    this.length = 0;
  }

  if (this.length === 0) {
    return;
  }

  const deletedElement = this[this.length - 1];
  delete this[this.length - 1];
  this.length -= 1;
  return deletedElement;
};

MyArray.from = function (obj, ...args) {
  const newarr = new MyArray();

  for (let i = 0; i < obj.length; i++) {
    newarr.push(obj[i]);

    if (args[0] && args[1]) {
      newarr[i] = args[0].call(args[1], obj[i]);
    } else if (args[0]) {
      newarr[i] = args[0](obj[i]);
    }
  }

  return newarr;
};

MyArray.prototype.map = function (callback, ...args) {
  const newArr = new MyArray();
  const curLength = this.length;
  const context = args[0] ? args[0] : this;

  for (let i = 0; i < curLength; i++) {
    if (i in this) {
      newArr.push(callback.call(context, this[i], i, this));
    } else {
      newArr.push(this[i]);
    }
  }
  return newArr;
};


MyArray.prototype.forEach = function (func, ...args) {
  const context = func.bind(args[0]);
  const newLength = this.length;

  for (let i = 0; i < newLength; i++) {
    if (i in this) {
      context(this[i], i, this);
    }
  }
};

MyArray.prototype.reduce = function (func, ...args) {
  const newLength = this.length;
  let accum = args.length ? args[0] : this[0];
  const firstIndex = args.length ? 0 : 1;


  if (!this.length && !args.length) {
    throw new TypeError();
  }

  for (let i = firstIndex; i < newLength; i++) {
    if (i in this) {
      accum = func(accum, this[i], i, this);
    }
  }
  return accum;
};

MyArray.prototype.filter = function (func, ...args) {
  const newArr = new MyArray();
  const newLength = this.length;

  for (let i = 0; i < newLength; i++) {
    const item = this[i];

    if (i in this) {
      if (func.call(args[0], item, i, this)) {
        newArr.push(item);
      }
    }
  }

  return newArr;
};

MyArray.prototype.sort = function (func) {
  const newLength = this.length;
  const instMyArray = new MyArray();
  const that = new MyArray(...this);

  if (!func) {
    for (let j = newLength - 1; j > 0; j--) {
      for (let i = 0; i < j; i++) {
        if (String(this[i]) > String(this[i + 1])) {
          const item = this[i];
          this[i] = this[i + 1];
          this[i + 1] = item;
        }
      }
    }
    return this;
  }

  for (let i = 1; i < newLength; i++) {
    instMyArray.push(func(this[i], this[i - 1]));
  }

  for (let i = 0; i < newLength; i++) {
    if (!func.length && instMyArray[i] === -1) {
      for (let i = that.length - 1, j = 0; i >= 0; i -= 1, j += 1) {
        this[j] = that[i];
      }
      return this;
    }
  }
};

MyArray.prototype.toString = function () {
  let retString = '';

  for (let i = 0; i < this.length; i++) {
    retString += `${this[i] || ''},`;
  }
  return retString.slice(0, -1);
};


MyArray.prototype[Symbol.iterator] = function () {
  let currentInxex = 0;
  const endIndex = this.length;

  return {
    next: () => {
      if (currentInxex < endIndex) {
        const current = currentInxex;
        currentInxex += 1;
        return {
          done: false,
          value: this[current]
        };
      } else {
        return {
          done: true
        };
      }
    }
  };
};

module.exports = MyArray;

